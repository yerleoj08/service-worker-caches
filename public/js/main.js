if('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('./sw_cached_pages.js')
            .then((register)=>{
                if (register.installing) {
                    console.log('Installing...');
                }
            })
            .catch( (error) => console.log(`Error ${error}`))
    })
}

function isOnline(){
    if ('navigator' in window) {
        alert(`Online Status: ${navigator.onLine}`)
    }
    
   
}