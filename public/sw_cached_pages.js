
const cacheName = 'v1';

self.addEventListener('install', function(e){
    e.waitUntil(
        caches
            .open(cacheName)
            .then(function(cache){
                cache.addAll([
                    'about.html',
                    'index.html',
                    'css/style.css',
                    'js/main.js'
                ])
            }).then(()=> self.skipWaiting())
    )
})

self.addEventListener('activate',  function(e){
    console.log("Cached: activated")
    e.waitUntil(
        caches.keys().then(cachesName => {
            return Promise.all(
                cachesName.map(cache => {
                    if (cache !== cacheName) {
                        console.log("Deleting old cache")
                        return caches.delete(cache)
                    }
                })
            )
        })
    )
})

self.addEventListener('fetch', function(e) {
    console.log("Fetching...");
    e.respondWith(
        fetch(e.request).catch(() => caches.match( res => res))
    )
})