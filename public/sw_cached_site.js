
const cacheName = 'v1';

self.addEventListener('install', function(e){
    
})

self.addEventListener('activate',  function(e){
    console.log("Cached: activated")
    e.waitUntil(
        caches.keys().then(cacheName => {
            return Promise.all(
                cacheName.map(cache => {
                    if (cache !== cacheName) {
                        console.log("Deleting old cache")
                        return caches.delete(cache)
                    }
                })
            )
        })
    )
})

self.addEventListener('fetch', function(e) {
    console.log("Fetching...");
    e.respondWith(
        fetch(e.request)
        .then(response => {
            const cacheClone = response.clone();
            caches
                .open(cacheName)
                .then(cache => {
                    cache.put(e.request, cacheClone)
                })
            return response;
        }).catch(err => caches.match(e.request).then( res => res))
    )
})